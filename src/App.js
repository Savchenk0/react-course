import React, { useState, useMemo } from "react";
import "./styles/App.css";
import PostList from "./components/PostList";
import PostForm from "./components/PostForm";
import MySelect from "./components/UI/select/MySelect";
import MyInput from "./components/UI/input/MyInput";

function App() {
	const [posts, setPosts] = useState([
		{
			id: 1,
			title: "Балупа",
			body: "ЖJavascript - язык программирования",
		},
		{
			id: 2,
			title: "Валупа",
			body: "ГJavascript - язык программирования",
		},
		{
			id: 3,
			title: "Алупа",
			body: "ДJavascript - язык программирования",
		},
	]);
	const [selectedSort, setSelectedSort] = useState("");
	const [searchQuery, setSearchQuery] = useState("");
	const sortedPosts = useMemo(() => {
		if (selectedSort) {
			return [
				...posts.sort((a, b) => a[selectedSort].localeCompare(b[selectedSort])),
			];
		}
		return posts;
	}, [selectedSort, posts]);
	const handleSubmit = (post) => {
		setPosts([...posts, { ...post }]);
	};
	const handlePostDelete = (index) => {
		const newPosts = [...posts];
		newPosts.splice(index, 1);
		setPosts(newPosts);
	};
	const sortPosts = (sort) => {
		setSelectedSort(sort);
	};
	return (
		<div className="App">
			<PostForm onSubmit={handleSubmit} />
			<hr style={{ margin: "15px 0" }} />
			<MyInput
				placeholder="Поиск"
				value={searchQuery}
				onChange={(event) => setSearchQuery(event.target.value)}
			/>
			<MySelect
				defaultValue="сортировка"
				value={selectedSort}
				onChange={sortPosts}
				options={[
					{ value: "title", name: "по названию" },
					{ value: "body", name: "по описанию" },
				]}
			/>
			{posts.length !== 0 ? (
				<PostList onPostDelete={handlePostDelete} posts={sortedPosts} />
			) : (
				<h1 style={{ textAlign: "center" }}>Постов по запросу не найдено</h1>
			)}
		</div>
	);
}

export default App;

import MyButton from "./UI/button/MyButton";
import MyInput from "./UI/input/MyInput";
import { useState } from "react";
export default function PostForm({ onSubmit }) {
	const [post, setPost] = useState({ title: "", body: "" });
	const calculateSubmitDisabled = () => {
		if (!post.title || !post.body) return true;
		return false;
	};
	const isSubmitDisabled = calculateSubmitDisabled();
	const handleTitleChange = (e) => {
		setPost({ ...post, title: e.target.value });
	};
	const handleBodyChange = (e) => {
		setPost({ ...post, body: e.target.value });
	};
	const addNewPost = (e) => {
		e.preventDefault();
		onSubmit({ ...post, id: Date.now() });
		setPost({ title: "", body: "" });
	};

	return (
		<form>
			<MyInput
				onChange={handleTitleChange}
				type="text"
				placeholder="Название поста"
				value={post.title}
			/>
			<MyInput
				type="text"
				placeholder="Описание поста"
				value={post.body}
				onChange={handleBodyChange}
			/>
			<MyButton disabled={isSubmitDisabled} onClick={addNewPost}>
				Создать пост
			</MyButton>
		</form>
	);
}

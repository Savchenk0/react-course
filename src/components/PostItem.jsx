import React from "react";
export default function PostItem({ number, post, onPostDelete }) {
	const { title, body } = post;
	return (
		<div className="post">
			<div className="post__content">
				<strong>
					{number}. {title}
				</strong>
				<div>{body}</div>
			</div>
			<div className="post__btns">
				<button onClick={() => onPostDelete(number - 1)}> Удалить</button>
			</div>
		</div>
	);
}

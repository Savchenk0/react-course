import React from "react";
import PostItem from "./PostItem";
export default function PostList(props) {
	const { posts, onPostDelete } = props;
	return (
		<>
			<h1 style={{ textAlign: "center" }}>Список постов</h1>
			{posts.map((post, index) => {
				return (
					<PostItem
						onPostDelete={onPostDelete}
						number={index + 1}
						post={post}
						key={post.id}
					/>
				);
			})}
		</>
	);
}
